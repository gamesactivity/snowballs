package com.gamesactivity.SnowBalls.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Vector2;
import com.gamesactivity.SnowBalls.Manager;
import com.gamesactivity.SnowBalls.Renderer;
import com.gamesactivity.SnowBalls.input.Control;
import com.gamesactivity.SnowBalls.units.Artur;

import static com.badlogic.gdx.Input.Keys;


/**
 * Created by dima on 29.08.2014.
 */

public class ShowScreen extends AShowScreen {
    Renderer renderer;

    public ShowScreen(Game game, boolean newGame) {
        super(game);

        renderer = new Renderer(newGame);

        Artur artur =
                new Artur(
                    "artur",
                    new Vector2(Manager.size.x * 0.20f, Manager.size.y * 0.2f),
                    1,
                    new Control(Keys.W, Manager.getInput().getButton(0)),
                    new Control(Keys.S, Manager.getInput().getButton(1)),
                    renderer
                );

        Artur oldArtur =
                new Artur(
                    "peter",
                    new Vector2(Manager.size.x * 0.80f, Manager.size.y * 0.2f),
                    0,
                    new Control(Keys.O, Manager.getInput().getButton(2)),
                    new Control(Keys.L, Manager.getInput().getButton(3)),
                    renderer
                );

        artur.setEnemy(oldArtur);
        oldArtur.setEnemy(artur);

        renderer.add(artur, oldArtur);
    }

    @Override
    public void render(float deltaTime) {
        renderer.render(deltaTime);
    }
}
