package com.gamesactivity.SnowBalls.units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gamesactivity.SnowBalls.Manager;
import com.gamesactivity.SnowBalls.Renderer;

/**
 * Created by dima on 30.08.2014.
 */
public class Snow implements IUnit {
    static final float MAX_VEL = 100.0f;
    static final float FIXED_TIMER = 3.0f;
    static final float FLOOR_TOP = 0.15f;
    static final float FLOOR_BOTTOM = 0.07f;

    private float floor;

    private Rectangle bounds = new Rectangle();
    private Vector2 vel;
    private Artur target;
    private Animation animFly;
    private Animation animDeath;
    private boolean active = true;

    private float stateTime = 0;
    private float fixedTime = FIXED_TIMER;

    public Snow(Vector2 pos, Vector2 impulse, Artur target) {
        initAnims();

        this.floor = Manager.size.y * Manager.random(FLOOR_BOTTOM, FLOOR_TOP);
        this.bounds.setCenter(pos);
        this.vel = new Vector2(impulse);
        this.target = target;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public void render(float deltaTime, Renderer renderer) {
        stateTime += deltaTime;

        if (!Manager.isFreeze()) {
            if (active) {
                move(deltaTime);
                check();
            }
        }

        renderAim(renderer);
    }

    private void check() {
        if (bounds.overlaps(target.getSoftZone())) {
            target.die();
        }
    }

    private void move(float deltaTime) {
        Vector2 accel = new Vector2(0, -Manager.GRAVITY);
        accel.scl(deltaTime);
        vel.add(accel);
        vel.scl(deltaTime);
        bounds.x += vel.x;
        bounds.y += vel.y;
        vel.scl(1.0f / deltaTime);

        if (bounds.y + bounds.height / 2 < floor - 0.01f) {
            active = false;
        }
    }

    private void renderAim(Renderer renderer) {
        Animation anim = (active ? animFly : animDeath);
        TextureRegion region = anim.getKeyFrame(stateTime, true);
        renderer.batch.draw(region, bounds.x, bounds.y, bounds.width, bounds.height);
    }

    private void initAnims() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("other.pack"));

        animFly = new Animation(
                0.5f,
                new TextureRegion(atlas.findRegion("snow"))
        );
        animDeath = new Animation(
                0.5f,
                new TextureRegion(atlas.findRegion("snow2"))
        );

        bounds.width = animFly.getKeyFrame(stateTime).getRegionWidth();
        bounds.height = animFly.getKeyFrame(stateTime).getRegionHeight();
    }
}
