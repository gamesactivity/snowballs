package com.gamesactivity.SnowBalls.units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gamesactivity.SnowBalls.Manager;
import com.gamesactivity.SnowBalls.Renderer;

/**
 * Created by dima on 30.08.2014.
 */
public class Aim implements IUnit {
    static final float MAX_VEL = 0.3f;
    static final float FIXED_TIMER = 1.0f;
    private final float BORDER = 0.2f;

    private boolean active = true;
    private Rectangle area;
    private Rectangle bounds = new Rectangle();
    private Vector2 fix;
    private Vector2 vel;
    private Animation anim;
    private float stateTime = 0;
    private float fixedTime = FIXED_TIMER;

    public Aim(Rectangle area) {
        initAnims();

        this.area = area;
        Vector2 pos = getRandomPoint();
        this.bounds.setCenter(pos);
        this.vel = new Vector2();
        this.fix = getRandomPoint();
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public void render(float deltaTime, Renderer renderer) {
        stateTime += deltaTime;

        if (!Manager.isFreeze()) {
            if (active) {
                move(deltaTime);
            }
        }

        renderAim(renderer);
    }

    private void move(float deltaTime) {
        if (fixedTime < 0.0f) {
            fix = getRandomPoint();
            fixedTime = FIXED_TIMER;
        }
        fixedTime -= deltaTime;

        Vector2 accel = new Vector2(fix);
        accel.sub(bounds.getCenter(new Vector2()));
        accel.scl(10.0f);

        deltaTime *= 1;

        accel.scl(deltaTime);
        vel.add(accel);
        vel.scl(deltaTime);
        vel.limit(MAX_VEL);
        bounds.x += vel.x;
        bounds.y += vel.y;
        vel.scl(1.0f / deltaTime);
    }

    private void renderAim(Renderer renderer) {
        TextureRegion region = anim.getKeyFrame(stateTime, true);
        renderer.batch.draw(region, bounds.x, bounds.y, bounds.width, bounds.height);
    }

    private void initAnims() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("other.pack"));

        anim = new Animation(
                0.5f,
                new TextureRegion(atlas.findRegion("aim")),
                new TextureRegion(atlas.findRegion("aim2"))
        );

        bounds.width = anim.getKeyFrame(stateTime).getRegionWidth();
        bounds.height = anim.getKeyFrame(stateTime).getRegionHeight();
        int r;

        String s = new String();
        StringBuilder sb = new StringBuilder();

    }

    private Vector2 getRandomPoint() {
        return new Vector2(
                getValueInRange(area.x, area.width, BORDER),
                getValueInRange(area.y, area.height, BORDER)
        );
    }

    private float getValueInRange(float bottom, float width, float border) {
        if (Math.random() > 0.5) {
            return Manager.random(bottom, bottom + width * border);
        }
        else {
            return Manager.random(bottom + width * (1.0f - border), bottom + width);
        }
    }
}
