package com.gamesactivity.SnowBalls.units;

import com.gamesactivity.SnowBalls.Renderer;

/**
 * Created by dima on 30.08.2014.
 */
public interface IUnit {
    public void render(float deltaTime, Renderer renderer);
}
