
package com.gamesactivity.SnowBalls.units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.SnowBalls.Manager;
import com.gamesactivity.SnowBalls.Renderer;
import com.gamesactivity.SnowBalls.input.Button;
import com.gamesactivity.SnowBalls.input.Control;

import java.util.HashMap;
import java.util.Map;

enum State {
    IDLE,
    JUMP,
    THROW,
    DYING,
    DEAD
}

public class Artur implements IUnit {
	private static final float JUMP_VELOCITY = 100;
    private static final float DEATH_VELOCITY = 70;
    private static final float THROW_TIMER = 0.2f;
    private static final float SOFT_COEFF = 0.5f;

    private int dir;
    private Aim aim;
    private Renderer renderer;
    private State state;
    private Vector2 ground;
    private Rectangle bounds = new Rectangle();
    private Rectangle soft = new Rectangle();
	private Vector2 vel;
    private Array<Map<State, Animation>> anims = new Array<Map<State, Animation>>();
    private float stateTime = 0;
    private float throwTimer = 0;
    private Artur enemy;
    private String name;

    Control controlJump;
    Control controlThrow;
    Buttons buttons;

	public Artur(String name, Vector2 ground, int dir, Control controlJump, Control controlThrow, Renderer renderer) {
        this.name = name;
        this.ground = new Vector2(ground);
        this.dir = dir;
        this.controlJump = controlJump;
        this.controlThrow = controlThrow;
        this.renderer = renderer;

        initAnims();

        this.bounds.setCenter(ground);
        this.soft.setCenter(this.bounds.getCenter(new Vector2()));
        this.vel = new Vector2();

        setupAim();
        changeState(State.IDLE);
	}

    private void setupAim() {
        float margin = Manager.size.x * 0.1f;

        Rectangle area = new Rectangle();
        area.width = Manager.size.x * 0.15f;
        area.height = Manager.size.y * 0.5f;
        area.x = Manager.size.x / 2 + (1 == dir ? - area.width - margin : margin);
        area.y = Manager.size.y * 0.2f;

        this.aim = new Aim(area);
        renderer.add(aim);
    }

    private Animation getAnim(TextureAtlas atlas, boolean flip, float period, String... names) {
        Array<TextureRegion> regions = new Array<TextureRegion>();
        for (String each : names) {
            regions.add(new TextureRegion(atlas.findRegion(each)));
            regions.peek().flip(flip, false);
        }

        return new Animation(period, regions);
    }

    private void initAnims() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(name + ".pack"));

        HashMap<State, Animation> left = new HashMap<State, Animation>();
        left.put(State.IDLE, getAnim(atlas, true, 0.8f, "stay", "stay2"));
        left.put(State.JUMP, getAnim(atlas, true, 0.2f, "jump", "jump2"));
        left.put(State.THROW, getAnim(atlas, true, 0.4f, "stay2", "throw", "throw2"));
        left.put(State.DYING, getAnim(atlas, true, 0.1f, "jump"));
        left.put(State.DEAD, getAnim(atlas, true, 0.1f, "stay2"));
        anims.add(left);

        HashMap<State, Animation> right = new HashMap<State, Animation>();
        right.put(State.IDLE, getAnim(atlas, false, 0.8f, "stay", "stay2"));
        right.put(State.JUMP, getAnim(atlas, false, 0.2f, "jump", "jump2"));
        right.put(State.THROW, getAnim(atlas, false, 0.4f, "stay2", "throw", "throw2"));
        right.put(State.DYING, getAnim(atlas, false, 0.1f, "jump"));
        right.put(State.DEAD, getAnim(atlas, false, 0.1f, "stay2"));
        anims.add(right);

        bounds.width = left.get(State.IDLE).getKeyFrame(stateTime).getRegionWidth();
        bounds.height = left.get(State.IDLE).getKeyFrame(stateTime).getRegionHeight();

        soft.width = bounds.width * SOFT_COEFF;
        soft.height = bounds.height * SOFT_COEFF;
    }

    @Override
	public void render(float deltaTime, Renderer renderer) {
        stateTime += deltaTime;

        if (!Manager.freeze || State.DYING == state) {
            if (State.IDLE == state) {
                processKeys(deltaTime);
            }

            if (State.JUMP == state || State.DYING == state) {
                processMove(deltaTime);
            }
        }

        renderArtur();
    }

    private void processKeys(float deltaTime) {
        if (controlJump.isPressed()) {
            vel.y = JUMP_VELOCITY;
            changeState(State.JUMP);
        }
        else if (controlThrow.isPressed()) {
            if (throwTimer < 0.0f) {
                throwTimer = THROW_TIMER;
                changeState(State.THROW);
            }
            else {
                throwTimer -= deltaTime;
            }
        }
    }

    private void processMove(float deltaTime) {
        Vector2 gravity = new Vector2(0, -Manager.GRAVITY);
        deltaTime *= 1.5f;

        gravity.scl(deltaTime);
        vel.add(gravity);
        vel.scl(deltaTime);

        offset(vel);
        vel.scl(1.0f / deltaTime);

        if (bounds.y + bounds.height / 2 < ground.y - 0.01f) {
            if (State.DYING == state) {
                Manager.dead(dir);
            }

            move(ground);
            changeState(State.IDLE);
        }
    }

    private void move(Vector2 vel) {
        bounds.setCenter(vel);
    }

    private void offset(Vector2 vel) {
        bounds.x += vel.x;
        bounds.y += vel.y;
    }

    private void renderArtur() {
        boolean loop = ((State.IDLE == state || State.JUMP == state) ? true : false);

        Animation anim = anims.get(dir).get(state);

        if (stateTime > anim.getAnimationDuration()) {
            handleAnimEnd();
        }

        TextureRegion reg = anim.getKeyFrame(stateTime, loop);
        renderer.batch.draw(reg, bounds.x, bounds.y, bounds.width, bounds.height);
    }

    private void handleAnimEnd() {
        if (State.THROW == state) {
            throwSomething();
            changeState(State.IDLE);
        }
    }

    private void throwSomething() {
        Vector2 from = bounds.getCenter(new Vector2());
        Vector2 impulse = aim.getBounds().getCenter(new Vector2());
        impulse.sub(from);

        Vector2 base = new Vector2(impulse);
        base.nor();
        base.scl(30.0f);
        impulse.scl(2.0f);
        impulse.add(base);

        Snow snow = new Snow(
                from,
                impulse,
                enemy
            );

        renderer.add(snow);
    }

    private void changeState(State newState) {
        stateTime = 0;
        state = newState;
    }

    public void die() {
        changeState(State.DYING);

        vel.y = DEATH_VELOCITY;
        ground.y = -bounds.height;
        
        Manager.freeze();
    }

    public final static class Buttons {
        public Button jump;
        public Button thrw;

        public Buttons(Button jump, Button thrw) {
            this.jump = jump;
            this.thrw = thrw;
        }
    }

    public void setEnemy(Artur enemy) {
        this.enemy = enemy;
    }

    public Rectangle getBounds(){
        return bounds;
    }

    public Rectangle getSoftZone(){
        return soft.setCenter(bounds.getCenter(new Vector2()));
    }
}
