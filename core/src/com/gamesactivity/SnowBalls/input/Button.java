package com.gamesactivity.SnowBalls.input;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by dima on 31.08.2014.
 */

public class Button {
    private int pointer;
    private TextureRegion region;
    private Rectangle rect;
    private boolean pressed;

    public Button(TextureRegion region, Rectangle rect) {
        this.region = region;
        this.rect = rect;
        this.pressed = false;
        this.pointer = -1;
    }

    public void render(SpriteBatch batch) {
        batch.draw(region, rect.x, rect.y, rect.width, rect.height);
    }

    public boolean isPressed() {
        return pressed;
    }

    public boolean down(float screenX, float screenY, int pointer) {
        if (rect.contains(screenX, screenY)) {
            pressed = true;
            this.pointer = pointer;
            return true;
        }
        return false;
    }

    public boolean up(int pointer) {
        if (this.pointer == pointer) {
            pressed = false;
            return true;
        }
        return false;
    }
}