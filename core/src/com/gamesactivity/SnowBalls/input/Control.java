package com.gamesactivity.SnowBalls.input;

import com.badlogic.gdx.Gdx;

/**
 * Created by dima on 31.08.2014.
 */
public final class Control {
    private int code;
    private Button button;

    public Control(int code, Button button) {
        this.code = code;
        this.button = button;
    }

    public boolean isPressed() {
        return Gdx.input.isKeyPressed(code) || button.isPressed();
    }
}
