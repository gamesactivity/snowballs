package com.gamesactivity.SnowBalls.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.SnowBalls.Manager;

/**
 * Created by dima on 31.08.2014.
 */
public class Joystick implements InputProcessor {
    private Array<Button> buttons = new Array<Button>();

    public void add(Button... buttonsList) {
        for (Button each : buttonsList) {
            buttons.add(each);
        }
    }

    public void render(SpriteBatch batch) {
        for (Button each : buttons) {
            each.render(batch);
        }
    }

    public Button getButton(int index) {
        return buttons.get(index);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        float ratioX = Manager.size.x / Gdx.graphics.getWidth();
        float ratioY = Manager.size.y / Gdx.graphics.getHeight();

        boolean ret = false;
        for (Button each : buttons) {
            if (each.down(screenX * ratioX, Manager.size.y - screenY * ratioY, pointer)) {
                ret = true;
            }
        }
        return ret;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        boolean ret = false;
        for (Button each : buttons) {
            if (each.up(pointer)) {
                ret = true;
            }
        }
        return ret;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
