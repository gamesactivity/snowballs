package com.gamesactivity.SnowBalls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.gamesactivity.SnowBalls.units.IUnit;

/**
 * Created by dima on 29.08.2014.
 */
public class Renderer {
    public Vector2 size;
    public SpriteBatch batch;

    private OrthographicCamera cam;
    private SpriteCache cache;
    private Sprite sprite;
    private Array<IUnit> units = new Array<IUnit>();

    private final float SCORES_TOP = 0.9f;
    private final float SCORES_WIDTH = 0.3f;

    private Sprite borderLeft;
    private Sprite borderRight;
    private Sprite borderMiddle;
    private Sprite borderLeftFill;
    private Sprite borderRightFill;

    public Renderer(boolean newGame) {
        this.batch = new SpriteBatch(5460);
        this.sprite = new Sprite(new Texture(Gdx.files.internal(Manager.getBackgroundName())));
        this.size = new Vector2(sprite.getWidth(), sprite.getHeight());

        this.cam = new OrthographicCamera(size.x, size.y);
        this.cam.position.set(size.x / 2, size.y / 2, 0);
        this.cam.update();

        this.cache = new SpriteCache(32, false);
        this.sprite.setPosition(0, 0);

        initAnim();
    }

    public void render(float deltaTime) {
        renderCache();

        renderBatch(deltaTime);
    }

    private void renderCache() {
        cache.setProjectionMatrix(cam.combined);
        Gdx.gl.glDisable(GL20.GL_BLEND);
        cache.begin();

        cache.end();
    }

    private void renderBatch(final float deltaTime) {
        batch.setProjectionMatrix(cam.combined);
        batch.begin();

        sprite.draw(batch);
        for (IUnit each : units) {
            each.render(deltaTime, this);
        }

        renderGui();

        batch.end();
    }

    private void initAnim() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("other.pack"));

        borderLeft = new Sprite(atlas.findRegion("border_left"));
        borderRight = new Sprite(atlas.findRegion("border_right"));
        borderMiddle = new Sprite(atlas.findRegion("border_middle"));
        borderLeftFill = new Sprite(atlas.findRegion("border_left_fill"));
        borderRightFill = new Sprite(atlas.findRegion("border_right_fill"));

        updateScoreLabels();
    }

    private void updateScoreLabels() {
        float left = 0.5f - SCORES_WIDTH * 0.5f;
        float right = 0.5f + SCORES_WIDTH * 0.5f;
        float middle = left + SCORES_WIDTH * Manager.getScoresRatio();
        float leftFillWidth = middle - left;
        float rightFillWidth = right - middle;

        borderLeft.setBounds(
                size.x * left - borderLeft.getRegionWidth() / 2.0f,
                size.y * SCORES_TOP,
                borderLeft.getRegionWidth(),
                borderLeft.getRegionHeight()
        );
        borderRight.setBounds(
                size.x * right - borderRight.getRegionWidth() / 2.0f,
                size.y * SCORES_TOP,
                borderRight.getRegionWidth(),
                borderRight.getRegionHeight()
        );
        borderMiddle.setBounds(
                size.x * middle - borderMiddle.getRegionWidth() / 2.0f,
                size.y * SCORES_TOP,
                borderMiddle.getRegionWidth(),
                borderMiddle.getRegionHeight()
        );
        borderLeftFill.setBounds(
                size.x * left + borderLeft.getRegionWidth() / 2.0f,
                size.y * SCORES_TOP,
                size.x * leftFillWidth - borderLeft.getRegionWidth() / 2.0f - borderMiddle.getRegionWidth() / 2.0f,
                borderLeftFill.getRegionHeight()
        );
        borderRightFill.setBounds(
                size.x * middle + borderMiddle.getRegionWidth() / 2.0f,
                size.y * SCORES_TOP,
                size.x * rightFillWidth - borderMiddle.getRegionWidth() / 2.0f - borderRight.getRegionWidth() / 2.0f,
                borderRightFill.getRegionHeight()
        );
    }

    private void renderGui() {
        borderLeft.draw(batch);
        borderRight.draw(batch);
        borderMiddle.draw(batch);
        borderLeftFill.draw(batch);
        borderRightFill.draw(batch);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Manager.getInput().render(batch);
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void add(IUnit... units) {
        for (IUnit each : units) {
            this.units.add(each);
        }
    }
}
