package com.gamesactivity.SnowBalls;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gamesactivity.SnowBalls.input.Button;
import com.gamesactivity.SnowBalls.input.Joystick;

public class SnowBalls extends Game {

	@Override
	public void create () {
        Texture background = new Texture(Gdx.files.internal("background0.png"));
        Manager.size = new Vector2(background.getWidth(), background.getHeight());

        Manager.setGame(this);
        createInput();

        Manager.restart(true);
	}

    private void createInput() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("other.pack"));
        TextureRegion region = atlas.findRegion("button");

        Vector2 size = Manager.size;
        float regWidth = region.getRegionWidth();
        float regHeight = region.getRegionHeight();

        Joystick joystick = new Joystick();
        joystick.add(
                new Button(
                        region,
                        new Rectangle(
                                size.x * 0.07f - regWidth / 2,
                                size.y * 0.75f - regHeight / 2,
                                regWidth,
                                regHeight
                        )
                ),
                new Button(
                        region,
                        new Rectangle(
                                size.x * 0.07f - regWidth / 2,
                                size.y * 0.25f - regHeight / 2,
                                regWidth,
                                regHeight
                        )
                ),
                new Button(
                        region,
                        new Rectangle(
                                size.x * 0.93f - regWidth / 2,
                                size.y * 0.75f - regHeight / 2,
                                regWidth,
                                regHeight
                        )
                ),
                new Button(
                        region,
                        new Rectangle(
                                size.x * 0.93f - regWidth / 2,
                                size.y * 0.25f - regHeight / 2,
                                regWidth,
                                regHeight
                        )
                )
        );

        Manager.setInput(joystick);
    }

    @Override
    public void pause() {
        Gdx.app.exit();
    }
}
