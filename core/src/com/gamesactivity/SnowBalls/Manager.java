package com.gamesactivity.SnowBalls;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.gamesactivity.SnowBalls.input.Joystick;
import com.gamesactivity.SnowBalls.screens.ShowScreen;

import java.util.Random;

/**
 * Created by dima on 30.08.2014.
 */
public class Manager {
    public static final float GRAVITY = 100.0f;
    public static Vector2 size = new Vector2();
    public static boolean freeze = false;

    private static Game game;
    private static Joystick input;
    private static final int MAX_SCORES = 6;
    private static int scores = MAX_SCORES / 2;
    private static String backgroundName;

    public static float random(float to) {
        return (float)Math.random() * to;
    }
    public static float random(float from, float to) {
        return random(to - from) + from;
    }

    public static boolean isFreeze() {
        return freeze;
    }

    public static void freeze() {
        freeze = true;
    }

    public static void restart(boolean newGame) {
        freeze = false;

        if (newGame) {
            refresh();
        }

        game.setScreen(new ShowScreen(game, newGame));
    }

    public static void setGame(Game game) {
        Manager.game = game;
    }

    public static void setInput(Joystick input) {
        Gdx.input.setInputProcessor(input);
        Manager.input = input;
    }

    public static Joystick getInput() {
        return input;
    }

    public static void dead(int dir) {
        boolean newGame = false;
        scores -= (dir * 2 - 1);
        if (0 >= scores || scores >= MAX_SCORES) {
            scores = MAX_SCORES / 2;
            newGame = true;
        }
        
        restart(newGame);
    }

    public static float getScoresRatio() {
        return (float)scores / MAX_SCORES;
    }

    private static void refresh() {
        Random random = new Random();
        backgroundName = "background" + random.nextInt(6) + ".png";
    }

    public static String getBackgroundName() {
        return backgroundName;
    }
}
